package com.lmungai.tictactoe;

import com.lmungai.tictactoe.exceptions.InvalidBoardException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;
import org.springframework.stereotype.*;

@Controller
@SpringBootApplication
public class TictactoeApplication {

	@RequestMapping("/")
	@ResponseBody
	String home(@RequestParam("board") String board) {
	    String boardResponse = "Placeholder for game board response";

		// check if board is valid
        if(board == null || board.isEmpty() || board.length() < 9 || board.length() > 9){
            throw new InvalidBoardException();
        } else{

            // process board contents
        }

		return boardResponse;
	}

	public static void main(String[] args) {
		SpringApplication.run(TictactoeApplication.class, args);
	}
}
