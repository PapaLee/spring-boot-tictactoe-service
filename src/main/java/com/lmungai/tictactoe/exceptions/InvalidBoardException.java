package com.lmungai.tictactoe.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.BAD_REQUEST, reason = "Invalid Tic-tac-toe Board")
public class InvalidBoardException extends RuntimeException {
    public InvalidBoardException(){

    }
}
